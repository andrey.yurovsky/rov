#pragma once

#include <unordered_map>
#include <array>
#include <functional>

#include <signal.h>
#include <sys/epoll.h>

class Event;

typedef std::function<bool(int, Event *)> cb_t;

class Event {
    public:
        Event();
        ~Event();
        int add(int fd, uint32_t events, cb_t cb);
        void remove(int fd);
        int add_signal(int signo, cb_t cb);
        void run();

    private:
        int epfd_;
        std::unordered_map<int, cb_t> callbacks_;

        std::array<cb_t, 32> signal_handlers_;
        int sfd_;
        sigset_t signals_mask_;
        bool signal_handler();
};
