#pragma once

#include "event.hh"

class ControlServer {
    public:
        ControlServer(Event &event, unsigned int port);
        ~ControlServer();

    private:
        int sfd_;
        Event &e_;
        static bool accept_client(int s, Event *e);
        static bool handle_client(int c, Event *e);
};
