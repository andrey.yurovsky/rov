#pragma once

#include <stdio.h>
#include "event.hh"

class Camera {
    public:
        Camera(Event &event, const char *path);
        ~Camera();
        int start_capture();
        void stop_capture();
        bool read_frame(int fd, Event *e);
    private:
        Event &e_;
        int fd_;
        void *buffer_start_;

        struct buffer {
            void *start;
            size_t length;
        };

        buffer buffers_[4];
        size_t num_buffers_;

        FILE *out_;
};
