#include <cassert>
#include <cstring>
#include <unistd.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>

#include "event.hh"

Event::Event()
{
    // Create an epoll interface that will not be inherited by child processes
    // (if any). This can only fail if we've hit a per-user limit on epoll
    // instances or a file descriptors limit.
    epfd_ = epoll_create1(EPOLL_CLOEXEC);
    if (epfd_ == -1) {
        throw std::runtime_error("epoll_create1(): " +
                std::string(std::strerror(errno)));
    }

    // Block the default handling of SIGTERM and SIGINT so we can install our
    // default signal handler.
    sigemptyset(&signals_mask_);
    sigaddset(&signals_mask_, SIGTERM);
    sigaddset(&signals_mask_, SIGINT);

    if (sigprocmask(SIG_BLOCK, &signals_mask_, NULL) == -1) {
        throw std::runtime_error("sigprocmask(): " +
                std::string(std::strerror(errno)));
    }

    // Create a signal file descritor. This will only fail if we've hit a file
    // descriptors limit or are out of memory.
    sfd_ = signalfd(-1, &signals_mask_, 0);
    if (sfd_ == -1) {
        throw std::runtime_error("signalfd(): " +
                std::string(std::strerror(errno)));
    }
    
    epoll_event e;

    e.data.fd = sfd_;
    e.events = EPOLLIN;

    // Monitor the signal file descriptor for reads (received signals). This
    // should not fail.
    if (epoll_ctl(epfd_, EPOLL_CTL_ADD, sfd_, &e)) {
        throw std::runtime_error("epoll_ctl(): " +
                std::string(std::strerror(errno)));
    }
}

Event::~Event()
{
    close(epfd_);
    close(sfd_);
    // Restore default handling of all signals that we had blocked.
    sigprocmask(SIG_UNBLOCK, &signals_mask_, NULL);
}

int Event::add_signal(int signo, cb_t cb)
{
    signal_handlers_[signo] = cb;
    if (!sigismember(&signals_mask_, signo)) {
        sigaddset(&signals_mask_, signo);

        int r = sigprocmask(SIG_BLOCK, &signals_mask_, NULL);
        if (r == -1) {
            return r;
        }
    }

    return signalfd(sfd_, &signals_mask_, 0) == sfd_ ? 0 : -1;
}

int Event::add(int fd, uint32_t events, cb_t cb)
{
    epoll_event e;

    e.data.fd = fd;
    e.events = events;

    int r = epoll_ctl(epfd_, EPOLL_CTL_ADD, fd, &e);
    if (r == 0) {
        callbacks_[fd] = cb;
    }

    return r;
}

void Event::remove(int fd)
{
    epoll_ctl(epfd_, EPOLL_CTL_DEL, fd, NULL);
    callbacks_.erase(fd);
}

void Event::run()
{
    bool is_running = true;

    while (is_running) {
        epoll_event events[4];

        int nfds = epoll_wait(epfd_, events, 4, -1);
        for (int i = 0; i < nfds; i++) {
            int efd = events[i].data.fd;
            if (efd == sfd_) {
                is_running = signal_handler();
            } else {
                cb_t cb = callbacks_[efd];
                is_running = cb == nullptr ? false : cb(efd, this);
            }
        }
    }
}

bool Event::signal_handler()
{
    signalfd_siginfo fdsi;

    ssize_t s = read(sfd_, &fdsi, sizeof(fdsi));
    if (s == sizeof(fdsi)) {
        assert(fdsi.ssi_signo < 32);
        cb_t cb = signal_handlers_[fdsi.ssi_signo];
        // Run the handler, if any
        return cb == nullptr ? false : cb(fdsi.ssi_signo, this);
    } else {
        // Keep going
        return true;
    }
}
