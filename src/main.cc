#include <iostream>
#include <string>
#include <stdexcept>
#include <cerrno>
#include <cassert>

#include "event.hh"
#include "controlserver.hh"
#include "camera.hh"

int sighup_handler(int signo, Event *e)
{
    std::cout << "SIGHUP" << std::endl;
    return true;
}

int main()
{
    Event e;

    ControlServer cs = ControlServer(e, 8080);

    e.add_signal(SIGHUP, sighup_handler);

    Camera c = Camera(e, "/dev/video0");
    c.start_capture();

    e.run();

    return 0;
}
