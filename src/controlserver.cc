#include <iostream>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "controlserver.hh"

bool ControlServer::handle_client(int c, Event *e)
{
    int sz = 0;
    ioctl(c, FIONREAD, &sz);

    if (sz == 0) {
        std::cout << "Client disconnected" << std::endl;
        close(c);
        e->remove(c);
    } else if (sz > 0) {
        // TODO
        char buf[128];
        if (static_cast<size_t>(sz) > sizeof(buf)) {
            sz = sizeof(buf);
        }
        std::cout << "Reading " << sz << " from client " << c << std::endl;
        read(c, buf, sizeof(buf));
    }

    return true;
}

bool ControlServer::accept_client(int s, Event *e)
{
    sockaddr_in ca;
    socklen_t len = sizeof(sockaddr_in);
    int c = accept(s, reinterpret_cast<sockaddr *>(&ca), &len);
    if (c == -1) {
        std::cerr << "accept(): " << std::strerror(errno) << std::endl;
    } else {
        char buf[INET_ADDRSTRLEN];
        std::cout << "connection from " <<
            inet_ntop(AF_INET, &ca.sin_addr, buf, sizeof(buf)) << std::endl;

        e->add(c, EPOLLIN, handle_client);
    }

    return true;
}

ControlServer::ControlServer(Event &event, unsigned int port) : e_(event)
{
    sfd_ = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK | SOCK_CLOEXEC,
            IPPROTO_TCP);
    if (sfd_ == -1) {
        throw std::runtime_error("socket(): " +
                std::string(std::strerror(errno)));
    }

    int val = 1;
    setsockopt(sfd_, SOL_SOCKET, SO_REUSEPORT, &val, sizeof(val));

    // Client sockets will inherit the SO_KEEPALIVE option
    val = 1;
    setsockopt(sfd_, SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val));

    sockaddr_in sa;

    sa.sin_family = AF_INET;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port = htons(port);

    if (bind(sfd_, reinterpret_cast<sockaddr *>(&sa), sizeof(sa))) {
        close(sfd_);
        sfd_ = -1;
        throw std::runtime_error("bind(): " +
                std::string(std::strerror(errno)));
    }

    if (listen(sfd_, 1)) {
        close(sfd_);
        sfd_ = -1;
        throw std::runtime_error("listen(): " +
                std::string(std::strerror(errno)));
    }

    e_.add(sfd_, EPOLLIN, accept_client);
}

ControlServer::~ControlServer()
{
    close(sfd_);
    e_.remove(sfd_);
    sfd_ = -1;
}
