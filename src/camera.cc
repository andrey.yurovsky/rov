#include <iostream>
#include <cstring>
#include <functional>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include <linux/videodev2.h>

#include "camera.hh"

bool Camera::read_frame(int fd, Event *e)
{
    std::cerr << "read" << std::endl;
    v4l2_buffer buf;
    
    memset(&buf, 0, sizeof(buf));
    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (ioctl(fd, VIDIOC_DQBUF, &buf) < 0) {
    } else {
        fwrite(buffers_[buf.index].start, buf.bytesused, 1, out_);
    }

    ioctl(fd, VIDIOC_QBUF, &buf);

    return true;
}

int Camera::start_capture()
{
    for (unsigned i = 0; i < num_buffers_; i++) {
        v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (ioctl(fd_, VIDIOC_QBUF, &buf) < 0) {
            throw std::runtime_error("ioctl(VIDIOC_QBUF,): " +
                    std::string(std::strerror(errno)));
        }
    }

    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd_, VIDIOC_STREAMON, &type) < 0) {
        std::cerr << "ioctl(VIDIOC_STREAMON)" <<
            std::string(std::strerror(errno)) << std::endl;
    }

    out_ = fopen("foo.bin", "w");

    auto cb = std::bind(&Camera::read_frame, this,
            std::placeholders::_1, std::placeholders::_2);
    e_.add(fd_, EPOLLIN, cb);

    return fd_;
}

void Camera::stop_capture()
{
    int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (ioctl(fd_, VIDIOC_STREAMOFF, &type) < 0) {
        std::cerr << "ioctl(VIDIOC_STREAMOFF)" <<
            std::string(std::strerror(errno)) << std::endl;
    }

    e_.remove(fd_);
}

Camera::Camera(Event &event, const char *path) : e_(event)
{
    fd_ = open(path, O_RDWR | O_CLOEXEC | O_NONBLOCK);
    if (fd_ == -1) {
        throw std::runtime_error("open(): " +
                std::string(std::strerror(errno)));
    }

    struct stat st;
    if (fstat(fd_, &st) || (st.st_mode & S_IFMT) != S_IFCHR) {
        close(fd_);
        throw std::runtime_error("not a character device");
    }

    v4l2_capability cap;
    if (ioctl(fd_, VIDIOC_QUERYCAP, &cap) < 0) {
        close(fd_);
        throw std::runtime_error("ioctl(VIDIOC_QUERYCAP): " +
                std::string(std::strerror(errno)));
    }

    if (!(cap.capabilities & (V4L2_CAP_VIDEO_CAPTURE | V4L2_CAP_STREAMING))) {
        close(fd_);
        throw std::runtime_error("device doesn't support video streaming");
    }

    v4l2_cropcap cropcap;

    memset(&cropcap, 0, sizeof(cropcap));
    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (ioctl(fd_, VIDIOC_CROPCAP, &cropcap) == 0) {
        v4l2_crop crop;
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect;

        ioctl(fd_, VIDIOC_S_CROP, &crop);
    }

    v4l2_format fmt;

    memset(&fmt, 0, sizeof(fmt));
    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    ioctl(fd_, VIDIOC_G_FMT, &fmt);

    v4l2_requestbuffers req;
    
    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (ioctl(fd_, VIDIOC_REQBUFS, &req) < 0) {
        close(fd_);
        throw std::runtime_error("ioctl(VIDIOC_REQBUFS): " +
                std::string(std::strerror(errno)));
    }

    if (req.count < 2 || req.count > 4) {
        close(fd_);
        throw std::runtime_error("insufficient buffer memory");
    }
    num_buffers_ = req.count;
    std::cerr << "Using " << num_buffers_ << " buffers" << std::endl;

    for (size_t i = 0; i < num_buffers_; i++) {
        v4l2_buffer buf;

        memset(&buf, 0, sizeof(buf));
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (ioctl(fd_, VIDIOC_QUERYBUF, &buf) < 0) {
            close(fd_);
            throw std::runtime_error("ioctl(VIDIOC_QUERYBUF): " +
                    std::string(std::strerror(errno)));
        }

        buffers_[i].length = buf.length;
        buffers_[i].start = mmap(NULL, buf.length, PROT_READ | PROT_WRITE,
                MAP_SHARED, fd_, buf.m.offset);
        if (buffers_[i].start == MAP_FAILED) {
            close(fd_);
            throw std::runtime_error("mmap(): " +
                    std::string(std::strerror(errno)));
        }
    }
}

Camera::~Camera()
{
    fclose(out_);
    close(fd_);
}
